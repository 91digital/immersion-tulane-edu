<?php

get_header();

?>

<!-- style -->

<style type="text/css">

 /*
  * media-queries
  */

  @media only screen and (max-width: 767px) {

  	/*
	 * above-the-fold
	 */
	 
	 #above-the-fold {
		 background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_video-placeholder-m.jpg") no-repeat;
		 background-position: 90% 0;
 		 background-size: cover;
	 }

	/*
	 * section-quote
	 */

	 #quote-ryan.section-quote {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_medical-expertise-m.jpg") no-repeat;
	 	background-position: center;
 		background-size: cover;
	 }

	 #quote-canaan.section-quote {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_infinite-solutions-m.jpg") no-repeat;
	 	background-position: center;
 		background-size: cover;
	 }

	 #quote-john.section-quote {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_business-driven-collaboration-m.jpg") no-repeat;
	 	background-position: center;
 		background-size: cover;
	 }

	/*
	 * section-in-residence
	 */

	 #section-in-residence {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-m.jpg") no-repeat;
	 	background-position: center top;
 		background-size: auto 100%;
	 }

  }

  @media only screen and (min-width: 768px) and (max-width: 1199px) {

  	/*
	 * above-the-fold
	 */
	 
	 #above-the-fold {
		 background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_video-placeholder-t.jpg") no-repeat;
		 background-position: center top;
 		 background-size: cover;
	 }

  	/*
 	 * section-text
 	 */

	 .section-text.section-text-1 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_medical-expertise-t.jpg") no-repeat;
	 	background-position: right;
 		background-size: auto 100%;
	 }

	 .section-text.section-text-2 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_infinite-solutions-t.jpg") no-repeat;
	 	background-position: left;
 		background-size: auto 100%;
	 }

	 .section-text.section-text-3 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_business-driven-collaboration-t.jpg") no-repeat;
	 	background-position: right;
 		background-size: auto 100%;
	 }

	 /*
	  * section-in-residence
	  */

	  #section-in-residence {
	 	 background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-t.jpg") no-repeat;
	 	 background-position: center top;
	 	 background-size: 100% auto;
	  }

  }

  @media only screen and (min-width: 1200px) {

  	/*
	 * above-the-fold
	 */
	 
	 #above-the-fold {
		 background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_video-placeholder-d.jpg") no-repeat;
		 background-position: center top;
 		 background-size: cover;
	 }

  	/*
 	 * section-text
 	 */

	 .section-text.section-text-1 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_medical-expertise-d.jpg") no-repeat;
	 	background-position: right;
 		background-size: auto 100%;
	 }

	 .section-text.section-text-2 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_infinite-solutions-d.jpg") no-repeat;
	 	background-position: left;
 		background-size: auto 100%;
	 }

	 .section-text.section-text-3 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_business-driven-collaboration-d.jpg") no-repeat;
	 	background-position: right;
 		background-size: auto 100%;
	 }

	 /*
	  * section-in-residence
	  */

	  #section-in-residence {
	 	 background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-d.jpg") no-repeat;
	 	 background-position: center top;
			 background-size: 100% auto;
	  }

  }

</style>

<!-- content -->

<div id="above-the-fold">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">

			<h1 class="main-headline">
				Scientific Collaboration at Business Speed
			</h1>

			<span id="office-name">
				Office of Research Business Development
			</span>

			<span class="main-subheadline">
				Your gateway to the next generation of cutting-edge medical expertise.
			</span>

			<a href="<?php echo get_permalink(get_page_by_title('Work With Us')); ?>" class="btn-cta">
				Work With Us Today
			</a>

		</div>
		
		<svg width="14px" height="8px" viewBox="0 0 14 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		    <g id="1---Home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		        <g id="1-1-1-Home-d" transform="translate(-633.000000, -687.000000)" stroke-width="1.5" stroke="#FFFFFF">
		            <g id="s-top">
		                <polyline id="arrow-down" points="646 688 640 694 634 688"></polyline>
		            </g>
		        </g>
		    </g>
		</svg>
		
	</div>

</div>

<div class="section-text section-text-1" id="medical-expertise">
	
	<div class="overlay-white"></div>

	<div class="container">
		
		<div class="row">

			<div class="col-lg-1"></div>

			<div class="col-xs-12 col-sm-7 col-md-6 col-lg-4 col-content">
				
				<h2 class="section-headline">
					Cutting edge medical expertise
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_tulane-shield-outline.svg">
				</h2>

				<p>
					Benefit from the wide range of research and facility strengths Tulane has to offer. Here are just a few areas in which we excel:
				</p>

				<ul>
					<li>Infectious Disease</li>
					<li>Cardiovascular Conditions</li>
					<li>Cancer Biology</li>
					<li>Neuroscience</li>
					<li>NHP Research</li>
					<li><a href="<?php echo get_permalink(get_page_by_title('Expertise')); ?>">More...</a></li>
				</ul>

				<p class="quote-text">
					“Tulane University Medical moves fast, startup speed fast, we’ve been <span>very impressed</span> by both their can-do business attitude to building startups and their emerging regenerative medicine technologies.”
				</p>

				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_ryan@2x.png" class="quote-image">

				<span class="quote-source">
					Ryan Bethencourt, IndieBio
				</span>

				<div class="clearfix"></div>

			</div>

		</div>

	</div>

</div>

<div class="section-quote" id="quote-ryan">
	
	<div class="overlay-green"></div>

	<div class="container">
		
		<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_ryan@2x.png">

		<p class="quote-text">
			“They move fast, startup speed fast, we’ve been <span>very impressed</span> by both their can-do business attitude to building startups and their emerging regenerative medicine technologies.”
		</p>

		<span class="quote-author">
			Ryan Bethencourt, IndieBio
		</span>

	</div>

</div>

<div class="section-text section-text-2" id="infinite-solutions">
	
	<div class="overlay-white"></div>

	<div class="container">
		
		<div class="row">

			<div class="col-sm-5 col-md-6 col-lg-7"></div>
			
			<div class="col-xs-12 col-sm-7 col-md-6 col-lg-4 col-content">
				
				<h2 class="section-headline">
					One gateway to infinite solutions
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_tulane-shield-outline.svg">
				</h2>

				<p>
					From exploring innovative research models to translating discoveries into products and services that will transform the medical community - we can help:
				</p>

				<ul>
					<li>Collaborative Research</li>
					<li>Contract Research</li>
					<li>Clinical Trials</li>
					<li>Specialized Resources</li>
					<li>Innovative Technologies</li>
					<li><a href="<?php echo get_permalink(get_page_by_title('About')); ?>">More...</a></li>
				</ul>

				<p class="quote-text">
					“Tulane sets a <span>great example</span> for institutions based outside the major clusters. James’ concerted effort to actively engage with the Boston/Cambridge ecosystem sets him and Tulane apart.”
				</p>

				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_john-2@2x.png" class="quote-image">

				<span class="quote-source">
					John Hallinan, Massachusetts Biotechnology Council
				</span>

				<div class="clearfix"></div>

			</div>

			<div class="col-lg-1"></div>

		</div>

	</div>

</div>

<div class="section-quote" id="quote-canaan">
	
	<div class="overlay-green"></div>

	<div class="container">
		
		<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_john-2@2x.png">

		<p class="quote-text">
			“Tulane sets a <span>great example</span> for institutions based outside the major clusters. James’ concerted effort to actively engage with the Boston/Cambridge ecosystem sets him and Tulane apart.”
		</p>

		<span class="quote-author">
			John Hallinan, Massachusetts Biotechnology Council
		</span>

	</div>

</div>

<div class="section-text section-text-3" id="collaboration">
	
	<div class="overlay-white"></div>

	<div class="container">
		
		<div class="row">

			<div class="col-lg-1"></div>

			<div class="col-xs-12 col-sm-7 col-md-6 col-lg-4 col-content">
				
				<h2 class="section-headline">
					Seamless business-driven collaboration
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_tulane-shield-outline.svg">
				</h2>

				<p>
					Get a custom-tailored game plan that focuses on your specific needs and goals. We’ll guide and support you every step of the way, so you can focus on working with our excellent scientists and state-of-the-art technologies.
				</p>

				<p class="quote-text">
					The Milken Institute Center for Strategic Philanthropy has worked with the Office of Research Business Development at Tulane many times. They have <span>helped us make critical connections</span> that have made us better at our job.
				</p>

				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_melissa-2@2x.png" class="quote-image">

				<span class="quote-source">
					Melissa L. Stevens, Milken Institute
				</span>

				<div class="clearfix"></div>

			</div>

		</div>

	</div>

</div>

<div class="section-quote" id="quote-john">
	
	<div class="overlay-green"></div>

	<div class="container">
		
		<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_melissa-2@2x.png">

		<p class="quote-text">
			The Milken Institute Center for Strategic Philanthropy has worked with the Office of Research Business Development at Tulane many times. They have <span>helped us make critical connections</span> that have made us better at our job.
		</p>

		<span class="quote-author">
			Melissa L. Stevens, Milken Institute
		</span>

	</div>

</div>

<div class="container" id="twitter-feed">
	
	<div class="row">
		
		<div class="col-xs-12">
	
			<?php echo do_shortcode("[custom-twitter-feeds]"); ?>

		</div>

	</div>

</div>

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_sales-pages.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_home.min.js"></script>

<?php

get_footer();

?>