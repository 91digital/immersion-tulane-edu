<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php bloginfo('name'); ?></title>
		<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicon.ico">

		<!-- jQUERY -->
		<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery-3.2.1.min.js"></script>		
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

		<?php wp_head(); ?>
	</head>

<body <?php body_class(); ?>>