<?php

get_header();

?>

<!-- style -->
<style type="text/css">

	/*
	 * media-queries
	 */

	 @media only screen and (max-width: 767px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_about-m.jpg") no-repeat;
		 	background-position: center bottom;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 768px) and (max-width: 1199px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_about-t.jpg") no-repeat;
		 	background-position: center bottom;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 1200px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_about-d.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: 100% auto;
	 	 }

	 }
	
</style><!-- /style -->

<!-- content -->
<div id="page-title-area">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">
		
			<span class="page-title">
				About
			</span>

			<h1 class="page-headline">
				Your Personal Tulane Concierge
			</h1>

		</div>

	</div>
	
</div>

<div class="container" id="page-content">

	<div class="row">
		
		<div class="col-xs-12">
			
			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/icons/ic_text-decoration.svg" id="ic-text-decoration">

			<span id="text-intro">
				We believe in challenging the way corporations and universities collaborate, by embracing an agile mindset, and offering custom-tailored guidance and support that operates at the speed of business.
			</span>

			<p>
				The Office of Research Business Development connects the scientific expertise and specialized resources of Tulane with industry and other private collaborators.  Our focus is learning about your needs  – and working with you to determine how Tulane might help solve the challenges you face to augment your goals.
			</p>

			<p>
				We offer guidance and support before, during and after a specific project.  This comprehensive approach enhances scientific collaborations and allows a relationship with Tulane to be more holistic than the typical university.
			</p>

			<p>
				Working in tandem with all operational and scientific departments, we enable external stakeholders to easily access our rich expertise.
			</p>

			<h1>
				Collaboration Opportunities
			</h1>

			<p>
				If you want to explore innovative research models that can elevate your business, find experts, augment your human capital – or translate discoveries into products and services that will transform the medical community – We can help.
			</p>

			<ul>
				<li>Collaborative Research</li>
				<li>Contract Research</li>
				<li>Clinical Trials</li>
				<li>Specialized Resources</li>
				<li>Innovative Technologies</li>
				<li>Industry Alliances &amp; Strategic Relationships</li>
				<li>Alliance Management</li>
				<li>Faculty Consulting</li>
				<li>Continuing Education</li>
				<li>Workforce Connections</li>
				<li>...or Anything Else You Might Need</li>
			</ul>

			<p>
				Invite us to start a dialogue with you, so we can learn about your current external needs and future development plans. Why?  Because we know you are busy -- and the more we learn about you, the better enabled we are to put the right pieces together as opportunities arise.
			</p>

			<p>
				You can find us at partnering events or scientific conferences, and we will even come right to your office for an in-person meeting.  So engage with us and let us handle the work of moving the academic process forward so you can focus on what is most important – your business activities.
			</p>

			<h1>
				Meet James, our Chief Business Officer 
			</h1>

			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_james.jpg" id="img-james">

			<p id="text-james-intro">
				James Zanewicz (’95, Tulane Law) returned to Tulane in 2015 after establishing the open science functions for the Howard Hughes Medical Institute’s Janelia Research Campus, one of the most innovative experiments in scientific research.
			</p>

			<div class="clearfix"></div>

			<p>
				He was drawn back to Tulane from HHMI by the opportunity to tackle a career-defining opportunity: to apply all that he learned at HHMI Janelia and throughout his career to establish and grow the Research Business Development group for the institution (and in the city) that is closest to his heart.
			</p>

			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_james-korea.jpg" id="img-james-korea">

			<p>
				In his position of Chief Business Officer, he serves as the principal business development strategist and engagement officer to industry, venture capital and other external collaborators.
			</p>

			<p>
				It is important to note that the Research Business Development Office does not operate in a silo, but works closely with all other departments at Tulane to ensure that you will receive an optimal enterprise experience.
			</p>

			<p style="font-weight: 700;">
				Quite simply, James is your “Tulane Concierge.”
			</p>

			<h1>
				Clay Christian, Business Development Officer
			</h1>

			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_clay.jpg" id="img-james">

			<p id="text-james-intro">
			Claiborne “Clay” Christian (Tulane School of Medicine, PhD '16) came back to Tulane in 2018 after working as a freelance scientific writer and consultant for multiple academic institutions (including Tulane) and a national technology transfer and consulting group. 
			</p>

			<div class="clearfix"></div>

			<p>
			As a Business Development Officer for Tulane University, he focuses on establishing and facilitating collaborations involving research at Tulane and industry, venture capital, and other external partners – ensuring that all parties have an optimal enterprise experience. 
			</p>

			<p>
			His scientific expertise is in Molecular Biology and Genetics, with special interests in Oncology and Infectious Disease. His service to the profession includes being a member of the inaugural Corporate Engagement Intensive course committee for AUTM and the Professional Development Committee of NACRO.
			</p>

		</div>

	</div>
	
</div><!-- /content -->

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_about.min.js"></script>

<?php

get_footer();

?>