<?php

get_header('cta-page');

?>

<!-- style -->
<style type="text/css">

	/*
	 * media-queries
	 */

	 @media only screen and (max-width: 767px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-m.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 768px) and (max-width: 991px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-t.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 992px) {

	 	/*
	 	 * page-content
	 	 */

	 	 #col-content {
	 	 	overflow-y: scroll;
	 	 }

	 	 #col-image {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-d.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }
	
</style><!-- /style -->

<!-- content -->
<div id="page-title-area">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">
		
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logos/logo_tulane-shield.svg">
			</a>

			<span class="page-title">
				Application Form
			</span>

			<h1 class="page-headline">
				In-Residence Immersion Program 
			</h1>

		</div>

	</div>
	
</div>

<div class="container" id="page-content">

	<div class="row">
		
		<div class="col-xs-12 col-md-5 col-lg-5" id="col-content">

			<a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logos/logo_tulane-color.svg" id="tulane-logo"></a>

			<h2>
				In-Residence Immersion Program Application
			</h2>

			<p>
				Please fill out the form below to apply for your spot in the program. James, our CBO, will read through your application and contact you as soon as possible.
			</p>

			<?php 

			$p = get_page_by_title('In-Residence Application');
 			
 			echo apply_filters('the_content', $p->post_content);

			?>
			
		</div>

		<div class="col-xs-12 col-md-7 col-lg-7" id="col-image">
			
			<div class="overlay-green"></div>

			<div class="client-quote">
				
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/icons/ic_text-decoration-white.svg" id="ic-text-decoration">

				<p class="quote">
					“Tulane’s <span>innovative</span> In-Residence Immersion Program allows for accessibility to  collaborators and business facilities that help keep my business running when traveling in the area.”
				</p>

				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_nick@2x.png" id="img-quote-author">

				<span id="author-name">
					Nick Pashos, BioAesthetics
				</span>

			</div>

		</div>

	</div>

<div><!-- /content -->

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_cta-page.min.js"></script>

<?php

get_footer('cta-page');

?>