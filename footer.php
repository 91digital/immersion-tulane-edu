<!-- site-footer -->
<footer class="site-footer">
	<div class="container">	

		<!-- scroll-to-top -->
		<div id="row-scroll-to-top">
			<svg id="svg-scroll-to-top" width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <rect id="scroll-to-top-rectangle" x="0.5" y="0.5" width="39" height="39" fill="" stroke="#285C4D" stroke-width="1"></rect>
			    <polyline id="scroll-to-top-arrow" fill="#FFFFFF" stroke="#285C4D" stroke-width="1.2" points="14 23 20 17 26 23"></polyline>
			</svg>
		</div><!-- /scroll-to-top -->

		<div class="row">

			<!-- footer-address col -->
			<div class="col-xs-12 col-sm-4" id="footer-address">
				<span class="section-title">
					Office Of Research Business Development
				</span>
				<span id="address">
					1430 Tulane Ave, #8512<br>
					Suite 1553<br>
					New Orleans, LA 70112<br>
					<a href="https://www.google.com/maps/place/Tulane+University+Medical+School">View on Google Map</a>
				</span>
					
				<!-- phone -->
				<div id="phone">
					<span>Phone : <a href="tel:+1-504-988-4286">+1 504-988-4286</a></span>
				</div>

				<!-- mobile -->
				<div id="mobile">
					<span>Mobile : <a href="tel:+1-504-919-3800">+1 504-919-3800</a></span>
				</div>

				<!-- email -->
				<div id="email">
					<a href="mailto:zanewicz@tulane.edu">zanewicz@tulane.edu</a>
				</div>

				<!-- privacy -->
				<div id="privacy">
					<a href="http://tulane.edu/privacy">Privacy Policy</a>
				</div>
			</div><!-- /footer-address-col -->

			<!-- footer-links-col -->
			<div class="col-xs-12 col-sm-4" id="footer-links">
				<span class="section-title">
					Other Tulane Connections
				</span>
				<a href="https://tulane.edu/">Tulane University</a><br>
				<a href="http://tulane.edu/som">School of Medicine</a><br>
				<a href="http://tulane.edu/tnprc/">Tulane National Primate Research Center</a><br>
				<a href="http://tulane.edu/asvpr">Office of Research</a><br>
				<a href="http://tulane.edu/ott/index.cfm">Technology Transfer</a><br>
				<a href="http://tulane.edu/asvpr/ora/">Sponsored Projects Administration</a><br>
				<a href="http://tulane.edu/counsel/">General Counsel</a><br>
				<a href="http://tulane.edu/externalaffairs/">University Relations &amp; Development</a><br>
				<a href="http://tulane.edu/giving/cfr/index.cfm">Corporate, Foundations &amp; Research Relations</a><br>
				<a href="http://tulane.edu/government_affairs/">Goverment &amp; Community Relations</a><br>
				<a href="http://tulane.edu/asvpr/iacuc/">IACUC</a><br>
				<a href="http://tulane.edu/asvpr/irb/">Human Research Protection Program</a><br>
				<a href="http://tulane.edu/hiretulane/">Recruit at Tulane</a>
			</div><!-- /footer-links-col -->

			<!-- footer-social-col -->
			<div class="col-xs-12 col-sm-4 footer-social" id="social-tulane">
				
				<span class="section-title">Connect With Tulane</span>
				<a href="https://twitter.com/EngageTulane" style="opacity: 1;">
					<!-- twitter -->
					<svg width="48px" height="48px" id="social-twitter" viewBox="0 0 48 48" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	                    <path d="M0.5,47.5 L47.5,47.5 L47.5,0.5 L0.5,0.5 L0.5,47.5 Z" class="social-container" stroke="#FFFFFF" fill="#285C4D"></path>
	                    <path d="M32.0053333,18.76 C31.376,19.064 30.7072,19.2608 29.9984,19.352 C30.7205333,18.8869333 31.2752,18.1546667 31.5376,17.2768 C30.8618667,17.7050667 30.1141333,18.0112 29.3173333,18.1904 C28.6832,17.4570667 27.7728,17 26.7722667,17 C24.8453333,17 23.2837333,18.6794667 23.2837333,20.7568 C23.2837333,21.0485333 23.3141333,21.3365333 23.3717333,21.608 C20.4778667,21.4586667 17.9034667,19.9573333 16.1813333,17.6896 C15.8826667,18.2426667 15.7114667,18.8869333 15.7114667,19.5776 C15.7114667,20.8773333 16.3264,22.0341333 17.264,22.6981333 C16.6912,22.6746667 16.1530667,22.5146667 15.6832,22.2309333 L15.6832,22.2821333 C15.6832,24.0986667 16.8858667,25.6144 18.4816,25.9653333 C18.1893333,26.0506667 17.88,26.0896 17.56,26.0896 C17.336,26.0896 17.1226667,26.0736 16.9077333,26.0224 C17.3498667,27.52 18.6394667,28.608 20.1653333,28.6304 C18.9712,29.6426667 17.4677333,30.2373333 15.8330667,30.2373333 C15.5504,30.2373333 15.272,30.2208 15,30.1850667 C16.5429333,31.2554667 18.3738667,31.8794667 20.3482667,31.8794667 C26.7648,31.8794667 30.2757333,26.1541333 30.2757333,21.1914667 C30.2757333,21.0325333 30.272,20.872 30.264,20.7109333 C30.9450667,20.1744 31.5376,19.5114667 32.0053333,18.76" class="social-icon" fill="#FFFFFF"></path>
					</svg>
				</a><!--
				--><a href="https://http://www.linkedin.com/school/6806/" style="opacity: 1;">
					<!-- linkedin -->
					<svg width="48px" height="48px" id="social-linkedin" viewBox="0 0 48 48" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	                    <path d="M0.5,47.5 L47.5,47.5 L47.5,0.5 L0.5,0.5 L0.5,47.5 Z" class="social-container" stroke="#FFFFFF" fill="#285C4D"></path>
	                    <path d="M32.1349333,31.8154667 L28.5685333,31.8154667 L28.5685333,26.6069333 C28.5685333,25.2437333 28.0112,24.3136 26.784,24.3136 C25.8458667,24.3136 25.3248,24.9413333 25.0816,25.5466667 C24.9909333,25.7637333 25.0042667,26.0661333 25.0042667,26.3696 L25.0042667,31.8154667 L21.4714667,31.8154667 C21.4714667,31.8154667 21.5173333,22.5898667 21.4714667,21.7509333 L25.0042667,21.7509333 L25.0042667,23.3306667 C25.2133333,22.64 26.3418667,21.6538667 28.1445333,21.6538667 C30.3786667,21.6538667 32.1349333,23.1018667 32.1349333,26.2192 L32.1349333,31.8154667 Z M17.8986667,20.4917333 L17.8762667,20.4917333 C16.7381333,20.4917333 16,19.7226667 16,18.7477333 C16,17.7536 16.7605333,17 17.9210667,17 C19.0810667,17 19.7946667,17.752 19.8176,18.7450667 C19.8176,19.72 19.0810667,20.4917333 17.8986667,20.4917333 Z M16.4069333,31.8154667 L19.552,31.8154667 L19.552,21.7509333 L16.4069333,21.7509333 L16.4069333,31.8154667 Z" class="social-icon" fill="#FFFFFF"></path>
					</svg>
				</a>

				<span class="section-title">Connect With James</span>
				<a href="https://twitter.com/Zanewicz" style="opacity: 1;">
					<!-- twitter -->
					<svg width="48px" height="48px" id="social-twitter" viewBox="0 0 48 48" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	                    <path d="M0.5,47.5 L47.5,47.5 L47.5,0.5 L0.5,0.5 L0.5,47.5 Z" class="social-container" stroke="#FFFFFF" fill="#285C4D"></path>
	                    <path d="M32.0053333,18.76 C31.376,19.064 30.7072,19.2608 29.9984,19.352 C30.7205333,18.8869333 31.2752,18.1546667 31.5376,17.2768 C30.8618667,17.7050667 30.1141333,18.0112 29.3173333,18.1904 C28.6832,17.4570667 27.7728,17 26.7722667,17 C24.8453333,17 23.2837333,18.6794667 23.2837333,20.7568 C23.2837333,21.0485333 23.3141333,21.3365333 23.3717333,21.608 C20.4778667,21.4586667 17.9034667,19.9573333 16.1813333,17.6896 C15.8826667,18.2426667 15.7114667,18.8869333 15.7114667,19.5776 C15.7114667,20.8773333 16.3264,22.0341333 17.264,22.6981333 C16.6912,22.6746667 16.1530667,22.5146667 15.6832,22.2309333 L15.6832,22.2821333 C15.6832,24.0986667 16.8858667,25.6144 18.4816,25.9653333 C18.1893333,26.0506667 17.88,26.0896 17.56,26.0896 C17.336,26.0896 17.1226667,26.0736 16.9077333,26.0224 C17.3498667,27.52 18.6394667,28.608 20.1653333,28.6304 C18.9712,29.6426667 17.4677333,30.2373333 15.8330667,30.2373333 C15.5504,30.2373333 15.272,30.2208 15,30.1850667 C16.5429333,31.2554667 18.3738667,31.8794667 20.3482667,31.8794667 C26.7648,31.8794667 30.2757333,26.1541333 30.2757333,21.1914667 C30.2757333,21.0325333 30.272,20.872 30.264,20.7109333 C30.9450667,20.1744 31.5376,19.5114667 32.0053333,18.76" class="social-icon" fill="#FFFFFF"></path>
					</svg>
				</a><!--
				--><a href="https://www.linkedin.com/in/zanewicz" style="opacity: 1;">
					<!-- linkedin -->
					<svg width="48px" height="48px" id="social-linkedin" viewBox="0 0 48 48" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	                    <path d="M0.5,47.5 L47.5,47.5 L47.5,0.5 L0.5,0.5 L0.5,47.5 Z" class="social-container" stroke="#FFFFFF" fill="#285C4D"></path>
	                    <path d="M32.1349333,31.8154667 L28.5685333,31.8154667 L28.5685333,26.6069333 C28.5685333,25.2437333 28.0112,24.3136 26.784,24.3136 C25.8458667,24.3136 25.3248,24.9413333 25.0816,25.5466667 C24.9909333,25.7637333 25.0042667,26.0661333 25.0042667,26.3696 L25.0042667,31.8154667 L21.4714667,31.8154667 C21.4714667,31.8154667 21.5173333,22.5898667 21.4714667,21.7509333 L25.0042667,21.7509333 L25.0042667,23.3306667 C25.2133333,22.64 26.3418667,21.6538667 28.1445333,21.6538667 C30.3786667,21.6538667 32.1349333,23.1018667 32.1349333,26.2192 L32.1349333,31.8154667 Z M17.8986667,20.4917333 L17.8762667,20.4917333 C16.7381333,20.4917333 16,19.7226667 16,18.7477333 C16,17.7536 16.7605333,17 17.9210667,17 C19.0810667,17 19.7946667,17.752 19.8176,18.7450667 C19.8176,19.72 19.0810667,20.4917333 17.8986667,20.4917333 Z M16.4069333,31.8154667 L19.552,31.8154667 L19.552,21.7509333 L16.4069333,21.7509333 L16.4069333,31.8154667 Z" class="social-icon" fill="#FFFFFF"></path>
					</svg>
				</a>

			</div><!-- /footer-social-col -->

		</div>
	</div>
</footer><!-- /site-footer -->

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/main.min.js"></script>

<?php wp_footer(); ?>
</body>
</html>