<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php bloginfo('name'); ?></title>
		<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicon.ico">

		<!-- jQUERY -->
		<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery-3.2.1.min.js"></script>		
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

		<?php wp_head(); ?>
	</head>

<body <?php body_class(); ?>>

	<style type="text/css">
		
	.site-header .logo-tulane {
		background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/logos/logo_tulane.svg");
	}

	</style>
	
	<!-- site-header -->
	<header class="site-header">
		
		<!-- container-header -->
		<div class="container" id="header">

			<a href="<?php echo get_home_url(); ?>">
				<div class="logo-tulane"></div>		
			</a>
			
			<!-- site-nav -->
			<nav class="site-nav site-nav-hidden">

				<?php 
				wp_nav_menu(
					array(
							'theme_location'  => 'primary',
							'menu_class' => 'visible-links'
							)
				); 
				?>

				<ul class="hidden-links hidden-links-hidden"></ul>
				
			</nav><!-- /site-nav -->

			<a href="<?php if (is_page('in-residence')) { echo get_permalink(get_page_by_title('In-Residence Application')); } else { echo get_permalink(get_page_by_title('Work With Us')); } ?>" class="btn-work-with-us"><?php if (is_page('in-residence')) { echo 'Apply For Your Spot'; } else echo 'Work With Us'; ?></a>

		</div><!-- /header-container -->

	</header><!-- /site-header -->