<?php

get_header();

?>

<?php

if( have_posts() ) the_post();

$thumb_id 		 = get_post_thumbnail_id($post->ID);
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url 		 = $thumb_url_array[0];

?>

<!-- style -->
<style type="text/css">

	.nav-li-news a {
		color: #71C5E8 !important;
	}

	#page-title-area {
 	 	background: url("<?php echo $thumb_url; ?>") no-repeat;
	 	background-position: center top;
		background-size: cover;
 	 }
	
</style><!-- /style -->

<!-- content -->
<div id="page-title-area">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">
		
			<span class="page-title">
				<?php the_category(); ?>
			</span>

			<h1 class="page-headline">
				<?php the_title(); ?>
			</h1>

			<span class="post-author">
				By <?php the_author(); ?>
			</span>

		</div>

	</div>
	
</div>

<div class="container" id="page-content">

	<div class="row">
		
		<div class="col-xs-12">

			<?php the_content(); ?>

		</div>

	</div>
	
</div><!-- /content -->

<?php

get_footer();

?>