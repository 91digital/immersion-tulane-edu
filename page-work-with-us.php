<?php

get_header('cta-page');

?>

<!-- style -->
<style type="text/css">

	/*
	 * media-queries
	 */

	 @media only screen and (max-width: 767px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_work-with-us-m.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 768px) and (max-width: 991px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_work-with-us-t.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 992px) {

	 	/*
	 	 * page-content
	 	 */

	 	 #col-image {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_work-with-us-d.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }
	
</style><!-- /style -->

<!-- content -->
<div id="page-title-area">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">
		
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logos/logo_tulane-shield.svg">
			</a>

			<span class="page-title">
				Work With Us
			</span>

			<h1 class="page-headline">
				Set Up a Meeting With Our Chief Business Officer 
			</h1>

		</div>

	</div>
	
</div>

<div class="container" id="page-content">

	<div class="row">
		
		<div class="col-xs-12 col-md-5 col-lg-5" id="col-content">

			<a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logos/logo_tulane-color.svg" id="tulane-logo"></a>

			<h2>
				SET UP A MEETING WITH OUR ENGAGEMENT TEAM
			</h2>

			<p>
				Use the calendar below to schedule a Skype/Phone call, or personal meeting with James or Clay. Is it urgent? Call James at <a href="tel:+1-504-919-3800">504-919-3800</a> or email him at <a href="mailto:zanewicz@tulane.edu">zanewicz@tulane.edu</a>.
			</p>

			<iframe src="https://calendly.com/tulane-concierge" style="width:100%; height:650px;" frameBorder="0" scrolling="yes"></iframe>
			
		</div>

		<div class="col-xs-12 col-md-7 col-lg-7" id="col-image">
			
			<div class="overlay-green"></div>

			<div class="client-quote">
				
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/icons/ic_text-decoration-white.svg" id="ic-text-decoration">

				<p class="quote">
					“We’ve had a <span>great experience</span>! Whenever we need something we simply ask, and they figure out who to put us in touch with, saving us time and effort.”
				</p>

				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_canaan@2x.png" id="img-quote-author">

				<span id="author-name">
					Canaan Heard, PROTAC Fitness
				</span>

			</div>

		</div>

	</div>

<div><!-- /content -->

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_cta-page.min.js"></script>

<?php

get_footer('cta-page');

?>