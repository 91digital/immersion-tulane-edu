<?php 

add_theme_support( 'post-thumbnails' );

add_filter('show_admin_bar', '__return_false');

function register_files() {

	wp_register_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );
	wp_register_style( 'style_sales-page', get_stylesheet_directory_uri() . '/css/style_sales-page.min.css' );
	wp_register_style( 'style_expertise', get_stylesheet_directory_uri() . '/css/style_expertise.min.css' );
	wp_register_style( 'style_about', get_stylesheet_directory_uri() . '/css/style_about.min.css' );
	wp_register_style( 'style_events', get_stylesheet_directory_uri() . '/css/style_events.min.css' );
	wp_register_style( 'style_news', get_stylesheet_directory_uri() . '/css/style_news.min.css' );
	wp_register_style( 'style_cta-pages', get_stylesheet_directory_uri() . '/css/style_cta-pages.min.css' );

}
add_action( 'init', 'register_files' );

function add_files() {

	wp_enqueue_style( 'bootstrap' );
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	// conditional stylesheet loading

	if ( is_page( 'home' ) || is_page( 'in-residence' ) ) {

		wp_enqueue_style( 'style_sales-page' );

	}

	if ( is_page( 'expertise' ) ) {

		wp_enqueue_style( 'style_expertise' );

	}

	if ( is_page( 'about' ) ) {

		wp_enqueue_style( 'style_about' );

	}

	if ( is_page( 'events-list' ) ) {

		wp_enqueue_style( 'style_events' );

	}

	if ( is_home() || is_page( 'news' ) ) {

		wp_enqueue_style( 'style_news' );

	}

	/*if ( is_single() ) {

		wp_enqueue_style( 'style_single' );

	}*/

	if ( is_page( 'work-with-us' ) || is_page( 'in-residence-application' ) ) {

		wp_enqueue_style( 'style_cta-pages' );

	}

}
add_action( 'wp_enqueue_scripts', 'add_files' );

// register navigation menus

register_nav_menus( array(

	'primary' => 'Main Navigation'

));

// using stylesheet path in js

function stylesheet_url_script() {

	wp_enqueue_script('pw-script', get_stylesheet_directory_uri() . '/js/script_sales-pages.min.js');
	wp_localize_script('pw-script', 'pw_script_vars', array(
			'stylesheet_url' => get_stylesheet_directory_uri()
		)
	);

}
add_action('wp_enqueue_scripts', 'stylesheet_url_script');

// facebook meta tags

function add_opengraph_markup() {
  if (is_single()) {
    global $post;
    if(get_the_post_thumbnail($post->ID, 'thumbnail')) {
      $thumbnail_id = get_post_thumbnail_id($post->ID);
      $thumbnail_object = get_post($thumbnail_id);
      $image = $thumbnail_object->guid;
    } else {
      // set default image
      $image = 'img_work-with-us-m.jpg';
    }
    //$description = get_bloginfo(‘description’);
    $description = substr(strip_tags($post->post_content),0,200) . '...';
?>
<meta property=“og:title” content=“<?php the_title(); ?>” />
<meta property=“og:type” content=“article” />
<meta property=“og:image” content=“<?=$image?>” />
<meta property=“og:url” content=“<?php the_permalink(); ?>” />
<meta property=“og:description” content=“<?=$description?>” />
<meta property=“og:site_name” content=“<?=get_bloginfo('name')?>” />

<?php
  }
}
add_action('wp_head', 'add_opengraph_markup');

?>