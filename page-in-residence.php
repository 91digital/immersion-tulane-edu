<?php

get_header();

?>

<!-- style -->

<style type="text/css">

/*
 * scroll-to-top
 */

 #svg-scroll-to-top #scroll-to-top-rectangle,
 #svg-scroll-to-top #scroll-to-top-arrow {
 	stroke: #FFF;
 	stroke-width: 1.5;
 }

 #svg-scroll-to-top.svg-hover #scroll-to-top-rectangle {
 	stroke: transparent;
 }

 /*
  * media-queries
  */

  @media only screen and (max-width: 767px) {

  	/*
	 * above-the-fold
	 */
	 
	 #above-the-fold {
		 background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-m.jpg") no-repeat;
		 background-position: center top;
 		 background-size: cover;
	 }

	/*
	 * section-quote
	 */

	 #quote-nick.section-quote {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-room-m.jpg") no-repeat;
	 	background-position: center;
 		background-size: cover;
	 }

	 #quote-ryan.section-quote {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_work-among-faculty-m.jpg") no-repeat;
	 	background-position: center;
 		background-size: cover;
	 }

	 #quote-melissa.section-quote {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_new-orleans-m.jpg") no-repeat;
	 	background-position: center;
 		background-size: cover;
	 }

  }

  @media only screen and (min-width: 768px) and (max-width: 1199px) {

  	/*
	 * above-the-fold
	 */
	 
	 #above-the-fold {
		 background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-t.jpg") no-repeat;
		 background-position: center top;
 		 background-size: cover;
	 }

  	/*
 	 * section-text
 	 */

	 .section-text.section-text-1 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-room-t.jpg") no-repeat;
	 	background-position: right;
 		background-size: auto 100%;
	 }

	 .section-text.section-text-2 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_work-among-faculty-t.jpg") no-repeat;
	 	background-position: left;
 		background-size: auto 100%;
	 }

	 .section-text.section-text-3 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_new-orleans-t.jpg") no-repeat;
	 	background-position: right;
 		background-size: auto 100%;
	 }

  }

  @media only screen and (min-width: 1200px) {

  	/*
	 * above-the-fold
	 */
	 
	 #above-the-fold {
		 background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-d.jpg") no-repeat;
		 background-position: center top;
 		 background-size: cover;
	 }

  	/*
 	 * section-text
 	 */

	 .section-text.section-text-1 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_in-residence-room-d.jpg") no-repeat;
	 	background-position: right;
 		background-size: auto 100%;
	 }

	 .section-text.section-text-2 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_work-among-faculty-d.jpg") no-repeat;
	 	background-position: left;
 		background-size: auto 100%;
	 }

	 .section-text.section-text-3 {
	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_new-orleans-d.jpg") no-repeat;
	 	background-position: right;
 		background-size: auto 100%;
	 }

  }

</style>

<!-- content -->

<div class="in-residence" id="above-the-fold">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">
			
			<span>New!</span>

			<h1 class="main-headline">
				Your Personal Tulane Office
			</h1>

			<span class="main-subheadline">
				Faster collaboration. Closer interaction. Full immersion.
			</span>

			<a href="<?php echo get_permalink(get_page_by_title('In-Residence Application')); ?>" class="btn-cta">
				Apply For Your Spot
			</a>

		</div>

		<svg width="14px" height="8px" viewBox="0 0 14 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		    <g id="1---Home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		        <g id="1-1-1-Home-d" transform="translate(-633.000000, -687.000000)" stroke-width="1.5" stroke="#FFFFFF">
		            <g id="s-top">
		                <polyline id="arrow-down" points="646 688 640 694 634 688"></polyline>
		            </g>
		        </g>
		    </g>
		</svg>
		
	</div>

</div>

<div class="section-text section-text-1 in-residence" id="medical-expertise">
	
	<div class="overlay-white"></div>

	<div class="container">
		
		<div class="row">

			<div class="col-lg-1"></div>

			<div class="col-xs-12 col-sm-7 col-md-6 col-lg-4 col-content">
				
				<h2 class="section-headline">
					Become part of the Tulane campus
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_tulane-shield-outline.svg">
				</h2>

				<p>
					Have your personal work-home available to you, furnished with your own desk, chair, conference table, printer, and computer - everything you need to focus on your work.
				</p>

				<p class="quote-text">
					“Tulane’s <span>innovative</span> In-Residence Immersion Program allows for accessibility to  collaborators and business facilities that help keep my business running when traveling in the area.”
				</p>

				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_nick@2x.png" class="quote-image">

				<span class="quote-source">
					Nick Pashos, BioAesthetics
				</span>

				<div class="clearfix"></div>

			</div>

		</div>

	</div>

</div>

<div class="section-quote" id="quote-nick">
	
	<div class="overlay-green"></div>

	<div class="container">
		
		<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_nick@2x.png">

		<p class="quote-text">
			“Tulane’s <span>innovative</span> In-Residence Immersion Program allows for accessibility to  collaborators and business facilities that help keep my business running when traveling in the area.”
		</p>

		<span class="quote-author">
			Nick Pashos, BioAesthetics
		</span>

	</div>

</div>

<div class="section-text section-text-2 in-residence" id="infinite-solutions">
	
	<div class="overlay-white"></div>

	<div class="container">
		
		<div class="row">

			<div class="col-sm-5 col-md-6 col-lg-7"></div>
			
			<div class="col-xs-12 col-sm-7 col-md-6 col-lg-4 col-content">
				
				<h2 class="section-headline">
					Work among our faculty
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_tulane-shield-outline.svg">
				</h2>

				<p>
					Schedule meetings with scientists in the same building where they are developing the next generation of research, patient treatments and biomedical innovation. 
				</p>

				<p>
					“Come by and see me” means just that. You can pop down the hall to visit the lab where the next scientific breakthrough is happening at that very moment. 
				</p>

				<p class="quote-text">
					“They brought Tulane University Medical School to Silicon Valley and we took notice. James is <span>the best</span> CBO I've met at any public or private university to work with!”
				</p>

				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_ryan@2x.png" class="quote-image">

				<span class="quote-source">
					Ryan Bethencourt, IndieBio
				</span>

				<div class="clearfix"></div>

			</div>

			<div class="col-lg-1"></div>

		</div>

	</div>

</div>

<div class="section-quote" id="quote-ryan">
	
	<div class="overlay-green"></div>

	<div class="container">
		
		<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_ryan@2x.png">

		<p class="quote-text">
			“They brought Tulane University Medical School to Silicon Valley and we took notice. James is <span>the best</span> CBO I've met at any public or private university to work with!”
		</p>

		<span class="quote-author">
			Ryan Bethencourt, IndieBio
		</span>

	</div>

</div>

<div class="section-text section-text-3 in-residence" id="collaboration">
	
	<div class="overlay-white"></div>

	<div class="container">
		
		<div class="row">

			<div class="col-lg-1"></div>

			<div class="col-xs-12 col-sm-7 col-md-6 col-lg-4 col-content">
				
				<h2 class="section-headline">
					Embrace the city of New Orleans
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_tulane-shield-outline.svg">
				</h2>

				<p>
					Continue your brainstorming over cafe’ au lait and beignets, a fantastic jazz set, dinner prepared by any of a myriad of James Beard&trade; nominated (and winning) chefs -- or experience culinary chemistry in the form of an innovative cocktail hour hand-crafted by one of our world-renowned “star-tenders.”
				</p>

				<p class="quote-text">
					“They are a pleasure to work with and <span>incredibly helpful</span> in navigating Tulane. Their office is extremely responsive and well informed.”
				</p>

				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_melissa@2x.png" class="quote-image">

				<span class="quote-source">
					Melissa Pearlman, Torreya Partners
				</span>

				<div class="clearfix"></div>

			</div>

		</div>

	</div>

</div>

<div class="section-quote" id="quote-melissa">
	
	<div class="overlay-green"></div>

	<div class="container">
		
		<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_melissa@2x.png">

		<p class="quote-text">
			“They are a pleasure to work with and <span>incredibly helpful</span> in navigating Tulane. Their office is extremely responsive and well informed.”
		</p>

		<span class="quote-author">
			Melissa Pearlman, Torreya Partners
		</span>

	</div>

</div><!-- /content -->

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_sales-pages.min.js"></script>

<?php

get_footer();

?>