<?php

get_header();

?>

<!-- style -->
<style type="text/css">

	/*
	 * media-queries
	 */

	 @media only screen and (max-width: 767px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_events-m.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 768px) and (max-width: 1199px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_events-t.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 1200px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_events-d.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: 100% auto;
	 	 }

	 }
	
</style><!-- /style -->

<!-- content -->
<div id="page-title-area">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">
		
			<span class="page-title">
				News
			</span>

			<h1 class="page-headline">
				Stay Current With Tulane And The Industry
			</h1>

		</div>

	</div>
	
</div>

<div class="container" id="page-content">

	<div class="row">
		
		<div class="col-xs-12">

			<?php echo do_shortcode("[custom-twitter-feeds]"); ?>
			
		</div>

	</div>
	
</div><!-- /content -->

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_events.js"></script>

<?php

get_footer();

?>