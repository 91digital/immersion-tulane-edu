<?php

/*
 * Template Name: Events Page Template
 */

get_header();

?>

<!-- style -->
<style type="text/css">

	/*
	 * media-queries
	 */

	 @media only screen and (max-width: 767px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_events-m.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 768px) and (max-width: 1199px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_events-t.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 1200px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_events-d.jpg") no-repeat;
		 	background-position: center;
 		 	background-size: 100% auto;
	 	 }

	 }
	
</style><!-- /style -->

<!-- content -->
<div id="page-title-area">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">
		
			<span class="page-title">
				Events
			</span>

			<h1 class="page-headline">
				Upcoming Industry Events Where You Can Find Us
			</h1>

		</div>

	</div>
	
</div>

<div class="container" id="page-content">

	<div class="row">
		
		<div class="col-xs-12">

			<p style="margin-bottom: 16px;">
				Below you’ll find a list of all upcoming biotech industry events that James, our Chief Business Officer, will be attending or speaking at. If you’re around, come by and say hi. He’ll be more than happy to talk to you about the industry or possible collaborations.
			</p>

			<?php 

			$p = get_page_by_title('Events');
 			
 			echo apply_filters('the_content', $p->post_content);

			?>

		</div>

	</div>
	
</div><!-- /content -->

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_events.js"></script>

<?php

get_footer();

?>