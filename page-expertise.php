<?php

get_header();

?>

<!-- style -->
<style type="text/css">
	
	/*
	 * media-queries
	 */

	 @media only screen and (max-width: 767px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_expertise-m.jpg") no-repeat;
		 	background-position: center top;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 768px) and (max-width: 1199px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_expertise-t.jpg") no-repeat;
		 	background-position: center top;
 		 	background-size: cover;
	 	 }

	 }

	 @media only screen and (min-width: 1200px) {

	 	/*
	 	 * page-title-area
	 	 */

	 	 #page-title-area {
	 	 	background: url("<?php bloginfo('stylesheet_directory'); ?>/assets/images/img_expertise-d.jpg") no-repeat;
		 	background-position: center 20%;
 		 	background-size: 100% auto;
	 	 }

	 }

</style><!-- /style -->

<!-- content -->
<div id="page-title-area">

	<div class="overlay-green"></div>

	<div class="container">

		<div class="row">
		
			<span class="page-title">
				Expertise
			</span>

			<h1 class="page-headline">
				Leading Science Meets Advanced Technology
			</h1>

		</div>

	</div>
	
</div>

<div class="container" id="page-content">

	<div class="row">
		
		<div class="col-xs-12">
			
			<p>
				Tulane has a breadth of research, innovations and facility strengths for use and collaboration across all stages of the research continuum - basic, applied, advanced and clinical.
			</p>

			<p>
				Below we spotlight a selection of areas in which we excel.  If you don’t see what you are looking for, please <a href="#" class="link-contact-us">contact us</a> directly with your inquiry to see if Tulane’s research capabilities and related expertise can meet your needs.
			</p>

			
			<!-- expertise-areas -->
			<div id="expertise-areas">

			<!-- Cancer: Biology -->
			<div class="expertise collapsed" id="expertise-cancer-biology" data-toggle="collapse" data-target=".expertise-cancer-biology.collapse">
					
					<span class="expertise-name">
						Cancer: Biology
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-cancer-biology expertise-content collapse">
						
						<h1>
							Cancer: Biology
						</h1>

						<p>
							Our cancer/oncology program spans boundaries — both within Tulane and at the joint enterprise of the Louisiana Cancer Research Center.
						</p>

						<p>
							Areas of research include molecular genetics, tumor biology and signaling, population sciences and prevention, clinical and translational research, cancer health disparities, circadian cancer, leukemia/lymphoma, and prostate, breast and gynecologic cancers.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Blask & Hill Labs: Holistic Oncology Approach</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/BlaskHill_Labs_CircadianCancerBiology_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg>
								</a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Cancer Center Bioinformatics Core</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Flemnington_Lab_CancerBiology_BioinformaticsAnalysis_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Gragert Lab: Population Genetices & Informatics for Transplantation</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Gragert_Lab_PopulationGenetics_InformaticsforTransplantation_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Lu Lab: Activation and Regulation of p53</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Lu_Lab_CancerBiology_p53Activation&Regulation_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Cancer Center Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Cancer_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Safah Lab: Stem Cell Transplants to Treat Blood Disorders</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Safah_Lab_ClinicalResearch_StemCellTransplantProgram_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Surgical Urology Cancer Group</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Silberstein_Group_ClinicalResearch_SurgicalUrologyCancerGroup_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Lee Lab: Modeling Rare Sarcomas & Stem Cells</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Lee_Lab_CancerBiology_UniqueAnimalModels&RareTumors_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Coy Lab: Peptide Chemistry & Design</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Coy_Lab_Biochemistry_PeptideChemistry&Design_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>


				</div><!-- /Cancer: Biology -->

			<!-- Cancer: Breast Cancer -->
			<div class="expertise collapsed" id="expertise-cancer-breast-cancer" data-toggle="collapse" data-target=".expertise-cancer-breast-cancer.collapse">
					
					<span class="expertise-name">
						Cancer: Breast Cancer
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-cancer-breast-cancer expertise-content collapse">
						
						<p>
							Our cancer/oncology program spans boundaries — both within Tulane and at the joint enterprise of the Louisiana Cancer Research Center.
						</p>

						<p>
							Areas of research include molecular genetics, tumor biology and signaling, population sciences and prevention, clinical and translational research, cancer health disparities, circadian cancer, leukemia/lymphoma, and prostate, breast and gynecologic cancers.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Cancer Center Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Cancer_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Machado Lab: Understanding the Early Stages</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Machado_Lab_CancerBiology_BreastCancer_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Burow & Collins-Burow Labs: Triple Negative Cancer from Biology to Bedside</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/BurowCollinsBurow_Labs_BreastCancerBiology_TNBC_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Jackson Lab: Senescence and Recurrent Cancer</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Jackson_Lab_CancerBiology_BreastCancer_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>


				</div><!-- /Cancer: Breast Cancer -->

			<!-- Cancer: DNA Damage and Repair -->
			<div class="expertise collapsed" id="expertise-dna-damage-and-repair" data-toggle="collapse" data-target=".expertise-dna-damage-and-repair.collapse">
					
					<span class="expertise-name">
						Cancer: DNA Damage and Repair
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-dna-damage-and-repair expertise-content collapse">
						
						<p>
							Our cancer/oncology program spans boundaries — both within Tulane and at the joint enterprise of the Louisiana Cancer Research Center.
						</p>

						<p>
							Areas of research include molecular genetics, tumor biology and signaling, population sciences and prevention, clinical and translational research, cancer health disparities, circadian cancer, leukemia/lymphoma, and prostate, breast and gynecologic cancers.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Cancer Center Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Cancer_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Belancio Lab: Environmental Influences</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Belancio_Lab_CancerBiology_EnvironmentalInfluences_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Deininger Lab: Retrotranspoon-Mediated DNA Damage & Repair</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Deininger_Lab_CancerBiology_DNADamage&Repair_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Han Lab: Retrotransposon-Mediated DNA Damage & Repair</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Han_Lab_DNADamage&Repair_RetrotransposonBiology_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Pursell Lab: Beyond Basic DNA Biochemistry</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Pursell_Lab_CancerBiology_DNA_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Jackson Lab: Senescence and Recurrent Cancer</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Jackson_Lab_CancerBiology_BreastCancer_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>


				</div><!-- /Cancer: DNA Damage and Repair -->

			<!-- Cancer: Prostate Cancer -->
			<div class="expertise collapsed" id="expertise-prostate-cancer" data-toggle="collapse" data-target=".expertise-prostate-cancer.collapse">
					
					<span class="expertise-name">
						Cancer: Prostate Cancer
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-prostate-cancer expertise-content collapse">

						<p>
							Our cancer/oncology program spans boundaries — both within Tulane and at the joint enterprise of the Louisiana Cancer Research Center.
						</p>

						<p>
							Areas of research include molecular genetics, tumor biology and signaling, population sciences and prevention, clinical and translational research, cancer health disparities, circadian cancer, leukemia/lymphoma, and prostate, breast and gynecologic cancers.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Dong Lab & Atomwise: AI Generated Cancer Drugs</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Dong_Lab_OncologyProstateCancer_AIDrugDiscovery_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Dong Lab: Castration-Resistant Prostate Cancer & Androgen Receptor Splice Variants</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Dong_Lab_CancerBiology_ProstateCancer_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Dr. Oliver Sartor: The Prostate Cancer Group</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Sartor_Group_ProstateCancer_ClinicalTrials_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">You Lab: Prostate Cancer & Inflammatory Signaling</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Youl_Lab_ProstateCancer_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Zhang Lab: Androgen Receptor Splice Variants</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Zhang_Lab_CancerBiology_ProstateCancer_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>


				</div><!-- /Cancer: Prostate Cancer -->

				<!-- Clinical Research -->
				<div class="expertise collapsed" id="expertise-clinical-research" data-toggle="collapse" data-target=".expertise-clinical-research.collapse">
					
					<span class="expertise-name">
						Clinical Research
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-clinical-research expertise-content collapse" >
						
						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Cancer Center Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Cancer_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Safah Lab: Stem Cell Transplants to Treat Blood Disorders</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Safah_Lab_ClinicalResearch_StemCellTransplantProgram_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Surgical Urology Cancer Group</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Silberstein_Group_ClinicalResearch_SurgicalUrologyCancerGroup_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Clinical Trials: Sports Medicine & Connective Tissue Research</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/ClinicalResearch_Orthopedics_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Center for Clinical Neurosciences Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Center_for_Clinical_Neurosciences_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>

				</div><!-- /Clinical Research -->

				<!-- CV and Metabolic Disorders -->
				<div class="expertise collapsed" id="expertise-cv-and-metabolic-disorders" data-toggle="collapse" data-target=".expertise-cv-and-metabolic-disorders.collapse">
					
					<span class="expertise-name">
						CV and Metabolic Disorders
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-cv-and-metabolic-disorders expertise-content collapse" >
						
						<p>
							Tulane is focused on the prevention and treatment of adverse events and conditions relating to the continuum of cardiovascular health, i.e.: Hypertension and Diabetes.
						</p>

						<p>
							We are also the home of the Bogalusa Heart Study, and the related wealth of clinical information and expertise derived from years of studying these conditions.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Batuman Lab: Kidney Biology & Disease</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Batuman_Lab_KidneyBiology&Disease_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Chen Lab: Clinical Trials in Kidney & CV Disease</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Chen_Lab_Nephrology_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Fonseca and Mauvais-Jarvis Labs: Diabetes Trials & Research</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/FonsecaMauvaisJarvis_Labs_DiabetesResearchPrograms_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Hering-Smith Lab: Kidney Function, Diabetes, and Citrate</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/HeringSmith_Lab_Nephrology_KidneyStones&Diabetes_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Krousel-Wood Lab: Caridovascular Disease and Medication</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/KrouselWood_Lab_ClinicalResearch_CardiovascularDisease_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Lindsey Lab: Estrogen Signaling in Cardiovascular Health</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Lindsey_Lab_CardiovascularDisease_EstrogenSignaling_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Mauvais-Jarvis Lab: Type 1 Diabetes Research</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/MauvaisJarvis_Lab_TypeIDiabetes_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Nakhoul Lab: Acid/Base Balance & Kidney Disease</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Nakhoul_Lab_Nephrology_AcidBaseBiology_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Prieto Lab: Kidney Biology & Hypertension</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Prieto_Lab_Nephrology_CVDisease_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Saifudeen Lab: Optimizing Nephron Endowment</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Saifudeen_Lab_Nephrology_KidneyDevelopment_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Woods Lab: Cardiovascular Disease and Co-Morbidities</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Woods_Lab_CardiovascularDisease_Diabetes&Stroke_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>
						

				</div><!-- /CV and Metabolic Disorders -->

				<!-- Immunology -->
				<div class="expertise collapsed" id="expertise-immunology" data-toggle="collapse" data-target=".expertise-immunology.collapse">
					
					<span class="expertise-name">
						Immunology
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-immunology expertise-content collapse">

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Steele Lab: Fungal Allergy and Asthma</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Steele_Lab_ImmunologyAsthma_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">McLachlan Lab</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/McLachlan_Lab_Immunology_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Pociask Lab: Immunology of Flu Infection</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Pociask_Flu_Immunology_Influenza_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">MacLean Lab: HIV in the CNS</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/MacLean_Lab_InfectiousDisease_CNS&HIV_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Braun Lab: Engineering T-Cells to Treat HIV</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Braun_Lab_Immunology_TCells&HIVTreatment_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">TNPRC</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_National_Primate_Research_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>


				</div><!-- /Immunology -->

				<!-- infectious-disease -->
				<div class="expertise collapsed" id="expertise-infectious-disease" data-toggle="collapse" data-target=".expertise-infectious-disease.collapse">
					
					<span class="expertise-name">
						Infectious Disease
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-infectious-disease expertise-content collapse" >
						
						<p>
							Tulane engages in a wide range of research activities relating to adult and pediatric infectious diseases, from clinical to epidemiological. Areas of strength include HIV/AIDS, ebola, mycoses, mycobacteria and tropical medicine.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Embers Lab: Lyme Diagnostic</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Embers_Lab_LymeDisease_Diagnostics_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Embers Lab: Lyme Disease</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Embers_Lab_LymeDisease_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Zwezdaryk Lab: Herpes and Cell Metabolism</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Zwezdaryk_Lab_InfectiousDisease_HerpesMetabolism_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Pociask Lab: Immunology of Flu Infection</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Pociask_Flu_Immunology_Influenza_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Kaur Lab: Chronic Viral Infection</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Kaur_Lab_InfectiousDisease_CMV&HIV_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Ling Lab: Eliminating HIV Reservoirs</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Ling_Lab_InfectiousDisease_HIV_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Panganiban Lab: RNA Viruses</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Panganiban_Lab_InfectiousDisease_RNAVirology_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">MacLean Lab: HIV in the CNS</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/MacLean_Lab_InfectiousDisease_CNS&HIV_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Bitoun Lab: Infectious Disease Vaccine Development</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Bitoun_Lab_InfectiousDisease_VaccineDevelopment_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Morici Lab: Pathogen/Host Interactions and Vaccinology</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Morici_Lab_InfectiousDisease_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Center for Translational Research in Infection and Inflammation</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Kolls_Lab_CenterforTranslationalResearchinInfectionandInflammation_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Zika Virus Research Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Zika_Virus_Research_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Braun Lab: Engineering T-Cells to Treat HIV</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Braun_Lab_Immunology_TCells&HIVTreatment_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">TNPRC</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_National_Primate_Research_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Wimley Lab: Applications of Membrane-Interacting Peptides</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Wimley_Lab_Biochemistry_PeptideChemistry_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>

				</div><!-- /infectious-disease -->

				<!-- Model Systems and Core Resources -->
				<div class="expertise collapsed" id="expertise-model-systems-and-core-resources" data-toggle="collapse" data-target=".expertise-model-systems-and-core-resources.collapse">
					
					<span class="expertise-name">
						Model Systems and Core Resources
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-model-systems-and-core-resources expertise-content collapse" >
						
						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Cancer Center Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Cancer_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Lee Lab: Modeling Rare Sarcomas & Stem Cells</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Lee_Lab_CancerBiology_UniqueAnimalModels&RareTumors_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Burow & Collins-Burow Labs: Triple Negative Cancer from Biology to Bedside</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/BurowCollinsBurow_Labs_BreastCancerBiology_TNBC_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Clinical Trials: Sports Medicine & Connective Tissue Research</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/ClinicalResearch_Orthopedics_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Circadian Animal Models</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/CircadianEffect_AnimalHousing_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Hayward Genetics Center</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Hayward_Genetics_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Center for Translational Research in Infection and Inflammation</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Kolls_Lab_CenterforTranslationalResearchinInfectionandInflammation_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">TNPRC</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_National_Primate_Research_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>

				</div><!-- /Model Systems and Core Resources -->

				<!-- neuroscience -->
				<div class="expertise collapsed" id="expertise-neuroscience" data-toggle="collapse" data-target=".expertise-neuroscience.collapse">
					
					<span class="expertise-name">
						Neuroscience
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-neuroscience expertise-content collapse" >
						
						<p>
							Neuroscience at Tulane is truly interdisciplinary, and focuses on research areas including neuroendocrinology, neurophysiology, neuropharmacology, and neurosurgery — as well as cognitive, behavioral and developmental neuroscience.
						</p>

						<p>
							Tulane is the home of an innovative teaching laboratory, molecular neuroscience core facilities, and a neurobiotechnology confocal microscopy core.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Mostany Lab: Real-Time Imaging of Neurons</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Mostany_Lab_Neuroscience_Aging&Brain Function_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Brain Institute Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Brain_Institute_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Tulane Center for Clinical Neurosciences Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Center_for_Clinical_Neurosciences_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>

				</div><!-- /neuroscience -->
				
				<!-- NHP-research -->
				<div class="expertise collapsed" id="expertise-nhp-research" data-toggle="collapse" data-target=".expertise-nhp-research.collapse">
					
					<span class="expertise-name">
						NHP Research
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-nhp-research expertise-content collapse">
						
						<p>
							Established in 1964, the Tulane National Primate Research Center (TNPRC) is one of only seven National Primate Research Centers, and is recognized as one of the country’s premier infectious disease research facilities.
						</p>

						<p>
							It functions as a regional and national resource for research using nonhuman primates; and is the site of one of 13 Regional Biosafety Laboratories (BSL3) in the NIH/NIAID National Biodefense Program.  It offers strengths in NHP research and model systems for a multitude of diseases.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Kaur Lab: Chronic Viral Infection</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Kaur_Lab_InfectiousDisease_CMV&HIV_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Ling Lab: Eliminating HIV Reservoirs</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Ling_Lab_InfectiousDisease_HIV_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Panganiban Lab: RNA Viruses</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Panganiban_Lab_InfectiousDisease_RNAVirology_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">MacLean Lab: HIV in the CNS</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/MacLean_Lab_InfectiousDisease_CNS&HIV_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Embers Lab: Lyme Disease</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Embers_Lab_LymeDisease_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Zika Virus Research Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Zika_Virus_Research_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Braun Lab: Engineering T-Cells to Treat HIV</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Braun_Lab_Immunology_TCells&HIVTreatment_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">TNPRC</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_National_Primate_Research_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Bunnell Lab: Cell & Gene Therapy</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Bunnell_Lab_Cell&GeneTherapy_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>


				</div><!-- /NHP-research -->

				<!-- peptides-chemistry -->
				<div class="expertise collapsed" id="expertise-peptides-chemistry" data-toggle="collapse" data-target=".expertise-peptides-chemistry.collapse">
					
					<span class="expertise-name">
						Peptides Chemistry
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-peptides-chemistry expertise-content collapse" >

						<p>
							Tulane has broad strengths in peptides research, from our Peptides Research Laboratories run by Dr. David Coy to the lab of Dr. Jim Zadina.
						</p>

						<p>
							The lab of James Zadina studies the societal epidemic of opioids and their role and mechanisms of action in alleviating pain and inducing side effects, currently research lines being based on modifications of naturally occurring peptide opioids.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Coy Lab: Peptide Chemistry & Design</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Coy_Lab_Biochemistry_PeptideChemistry&Design_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Coy Lab Overview</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Coy_Lab_TargetedTherapies_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Zadina Lab: Pain Therapeutics</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Zadina_Lab_PainTherapeutics_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
							
							<div class="pdf-item">
								<p class="pdf-item__text">Wimley Lab: Applications of Membrane-Interacting Peptides</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Wimley_Lab_Biochemistry_PeptideChemistry_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>

				</div><!-- /peptides-chemistry -->

				<!-- regenerative-medicine -->
				<div class="expertise collapsed" id="expertise-regenerative-medicine" data-toggle="collapse" data-target=".expertise-regenerative-medicine.collapse">
					
					<span class="expertise-name">
						Regenerative Medicine
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-regenerative-medicine expertise-content collapse" >

						<p>
							Tulane is a proven innovator in regenerative medicine. Research in this field includes cell therapy, gene therapy, tissue engineering and non-human primate research.
						</p>

						<p>
							New therapies for common diseases are developed including: osteoarthritis, stroke, diabetes, oncology and autoimmune diseases, and the TNPRC Division of Regenerative Medicine conducts research that utilizes vector-based gene therapies and cell-based interventions for genetic and acquired diseases.
						</p>

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">Bunnell Lab Cell &amp; Gene Therapy</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Bunnell_Lab_Cell&GeneTherapy_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">TNPRC</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_National_Primate_Research_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>

				</div><!-- /regenerative-medicine -->	

				<!-- Vaccinology -->
				<div class="expertise collapsed" id="expertise-vaccinology" data-toggle="collapse" data-target=".expertise-vaccinology.collapse">
					
					<span class="expertise-name">
						Vaccinology
					</span>

					<span class="expertise-toggle-icon">
						+
					</span>

					<div class="clearfix"></div>

				</div>

				<div class="expertise-vaccinology expertise-content collapse" >

						<div class="pdf-items">
							<div class="pdf-item">
								<p class="pdf-item__text">McLachlan Lab</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/McLachlan_Lab_Immunology_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Bitoun Lab: Infectious Disease Vaccine Development</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Bitoun_Lab_InfectiousDisease_VaccineDevelopment_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">Morici Lab: Pathogen/Host Interactions and Vaccinology</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Morici_Lab_InfectiousDisease_Tulane.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>

							<div class="pdf-item">
								<p class="pdf-item__text">TNPRC</p>
								<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_National_Primate_Research_Center_Overview.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
							</div>
						</div>

				</div><!-- /regenerative-medicine -->	

			</div><!-- /expertise-areas -->

			<div class="clearfix"></div>

			<h1>
				Didn’t find what you were looking for?
			</h1>

			<p>
				Take a look at our comprehensive Research Expertise Overview PDF:
			</p>

			<div class="clearfix"></div>

			<div class="pdf-items">
				<div class="pdf-item">
					<p class="pdf-item__text">Research Expertise Overview</p>
					<a class="pdf-item__download-button" href="<?php bloginfo('stylesheet_directory'); ?>/assets/documents/Tulane_Research_Resource_Guide_SYNERGY_2016.pdf" target="_blank"><span>Download PDF</span>
									<svg class="pdf-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M2 22H22" stroke="#418FDE" stroke-width="2" stroke-linecap="round"/>
										<path d="M12 18L4 10M12 18L20 10M12 18V2" stroke="#418FDE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg></a>
				</div>
			</div>

			<p class="contact-us-link-target">
				Still missing something? Talk to James Zanewicz, our Chief Business Officer or Clay Christian, Business Development Officer - your personal Tulane Concierge Desk. 
			</p>

			<div class="contact-us-row">
	 			<div class="contact-us_row__column">
				 	<p>
					  James Zanewicz
					</p>

					<p>
						Mobile : <a href="tel:+1-504-988-4286">+1 504-919-3800</a><br>
					</p>

					<p>
						Email : <a href="mailto:zanewicz@tulane.edu">zanewicz@tulane.edu</a>
					</p>
				</div>
				<div class="contact-us-row__column">

					<p>
						Clay Christian
					</p>

					<p>
						Mobile : <a href="tel:+1-504-988-4286">+1 504-909-3905</a><br>
					</p>

					<p>
						Email : <a href="mailto:zanewicz@tulane.edu">Christian@tulane.edu</a>
					</p>
				</div>
			</div>
		</div>

	</div>
	
</div><!-- /content -->

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/script_expertise.min.js"></script>

<?php

get_footer();

?>