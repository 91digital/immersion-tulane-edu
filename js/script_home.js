var touch = ("ontouchstart" in document.documentElement);

//
// above-the-fold-video
//

if (!touch) {

	$('#above-the-fold').css('background', 'none');
	
	$('<video playsinline autoplay muted loop poster="http://immersion.tulane.edu/wp-content/themes/immersion-tulane-edu/assets/images/img_video-placeholder-d.jpg" id="bgvid"><source src="' + pw_script_vars.stylesheet_url +  '/assets/videos/b-roll.webm" type="video/webm"><source src="' + pw_script_vars.stylesheet_url + '/assets/videos/b-roll.mp4" type="video/mp4"></video>').insertAfter('#above-the-fold .container');


}