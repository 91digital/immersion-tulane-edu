/*
 *
 */

var windowWidth = $( window ).width();
var touch = ("ontouchstart" in document.documentElement);

//
// content-replacement
//

if ( windowWidth >= 768 ) {

	$( '#page-title-area .page-headline' ).text( 'A Place Where Cutting-Edge Science Meets Advanced Technology' );

}

$( window ).on('resize', function() {

	windowWidth = $( window ).width();

	if ( windowWidth < 768 ) {

		$( '#page-title-area .page-headline' ).text( 'Leading Science Meets Advanced Technology' );

	}

	if ( windowWidth >= 768 ) {

		$( '#page-title-area .page-headline' ).text( 'A Place Where Cutting-Edge Science Meets Advanced Technology' );

	}

});


//
// hover-effects
//

$('.btn-pdf-download')
	.mouseover( function() {

		if (!touch) {

			$(this).addClass('btn-pdf-download-hover');

		}

	})
	.mouseout( function() {

		if (!touch) {

			$(this).removeClass('btn-pdf-download-hover');

		}

	});

//
// toggle-icon-effect
//

$( '#expertise-areas .expertise' ).on('click', function() {

	var iconText   = $(this).find('.expertise-toggle-icon').text().trim();
	var iconObject = $(this).find('.expertise-toggle-icon');

	if ( iconText == '+' ) {

		$(iconObject).text('-');

	} else {

		$(iconObject).text('+');

	}

});

//
// toggle-expertise-content
//

$('.expertise').on('click', function() {

	var object = $(this);

	if ($(object).hasClass('open')) {
		$(object).removeClass('open');
		$(object).addClass('collapsed');
		$(object).css('border-bottom-color', '#F0F0F0');
	} else {
		$(object).addClass('open');
		$(object).removeClass('collapsed');
		$(object).css('border-bottom-color', '#FFFFFF');
	}

});

//
// contact-us-scroll-link
//

$('.link-contact-us').on('click', function(event) {

	event.preventDefault();

	$('html, body').animate({scrollTop: $('.contact-us-link-target').offset().top }, 1000);

});