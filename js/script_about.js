
var windowWidth = $( window ).width();
var touch 		= ("ontouchstart" in document.documentElement);

//
// content-replacement
//

if ( windowWidth < 768 ) {

	$( '#page-title-area .page-headline' ).text( 'Your Personal Tulane Concierge' );

}

if ( windowWidth >= 768 ) {

	$( '#page-title-area .page-headline' ).text( 'Think of us as Your Personal Tulane Concierge' );

}

$( window ).on('resize', function() {

	windowWidth = $( window ).width();

	if ( windowWidth < 768 ) {

		$( '#page-title-area .page-headline' ).text( 'Your Personal Tulane Concierge' );

	}

	if ( windowWidth >= 768 ) {

		$( '#page-title-area .page-headline' ).text( 'Think of us as Your Personal Tulane Concierge' );

	}

});