// defining variables

	var header = $('.site-header');
	var logo   = $('.site-header .logo-tulane-shield');
	var btn	   = $('.site-nav .btn-more');
	var vlinks = $('.site-nav .visible-links');
	var hlinks = $('.site-nav .hidden-links');
	var cta    = $('.site-nav .btn-cta');

	var breaks = [];

	// the function responsible for the main nav responsiveness

	function updateNav() {

		//var availableSpace = header.width() - cta.width() - logo.width() - btn.width();


		var availableSpace = header.width();

		console.log('header width: ' + header.width());
		console.log('logo width: ' + logo.width());
		console.log('available space: ' + availableSpace);
		console.log('old vlinks width: ' + vlinks.width());

		// subtracting btn width from vlinks width since btn is part of the vlinks DOM
		var vlinksWidth = vlinks.width();

		console.log('new vlinks width: ' + vlinksWidth);

		if ( vlinksWidth > availableSpace ) {

			console.log('the available space is smaller than the vlinks div');

		} else {

			console.log('the available space is bigger than the vlinks div');

		}

		// IF main nav width < the available space -> THEN move second to last link
		// to the dropdown menu

		if (vlinksWidth >= availableSpace) {

			breaks.push(vlinks.width());

			vlinks.children().last().prev().prependTo(hlinks);

			if (btn.hasClass('btn-hidden')) {

				btn.removeClass('btn-hidden');

			}

		}else {

			// IF the available space > the recorded width of the links
			// in the sropdown menu -> THEN move the first drop down menu link
			// back into the main nav

			if (availableSpace > breaks[breaks.length-1]) {

				hlinks.children().first().insertAfter(vlinks.children().last().prev());
				breaks.pop();

			}

			// If there is no more link in the drop down menu -> THEN hide it

			if (breaks.length < 1) {

				btn.addClass('btn-hidden');
				hlinks.addClass('links-hidden');

			}

		}

		if (vlinksWidth > availableSpace) {

			updateNav();

		}

	}

	// IF the browser window is resized -> THEN call the updateNav() function

	$(window).resize( function() {

		updateNav();

	});

	btn.on('click', function() {

		hlinks.toggleClass('links-hidden');

	});

	updateNav();
	