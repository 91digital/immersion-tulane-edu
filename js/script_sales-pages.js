/*
 *
 */

var touch = ("ontouchstart" in document.documentElement);
var windowWidth = $( window ).width();

//
// above-the-fold scroll down
//

$('#above-the-fold svg').on('click', function(event) {

	/*event.preventDefault();*/

	$('html, body').animate({scrollTop: $('.section-text-1').offset().top }, 1000);

});

//
// hover effects
//

$('.btn-cta')
		.mouseover( function() {

			if (!touch) {

				$(this).addClass('btn-cta-hover');

			}

		})
		.mouseout( function() {

			if (!touch) {
				
				$(this).removeClass('btn-cta-hover');

			}

		});

//
// content-replacement
// replacing content within the sales page based on the window width
//

if ( windowWidth >= 768 ) {

	// homepage-sales-page

	$('#medical-expertise.section-text ul li:nth-of-type(2)').text('Cardiovascular');
	/*$('#infinite-solutions.section-text ul li:nth-of-type(1)').text('Collab Research');*/
	$('#infinite-solutions.section-text ul li:nth-of-type(5)').text('Technologies');

	$('#above-the-fold #office-name').insertBefore('#above-the-fold .main-headline');

}

if ( windowWidth >= 1200 ) {

	// homepage-sales-page

	$('#section-in-residence .main-headline').text('What if you could work with us from your personal Tulane office?');
}


$( window ).on( 'resize', function() {

	windowWidth = $( window ).width();

	if ( windowWidth < 768 ) {

		// homepage-sales-page

		$('#medical-expertise.section-text ul li:nth-of-type(2)').text('Cardiovascular Conditions');
		$('#infinite-solutions.section-text ul li:nth-of-type(1)').text('Collaborative Research');
		$('#infinite-solutions.section-text ul li:nth-of-type(5)').text('InnovativeTechnologies');

		$('#above-the-fold #office-name').insertAfter('#above-the-fold .main-headline');

	}

	if ( windowWidth >= 768 ) {

		// homepage-sales-page

		$('#medical-expertise.section-text ul li:nth-of-type(2)').text('Cardiovascular');
		$('#infinite-solutions.section-text ul li:nth-of-type(1)').text('Collab Research');
		$('#infinite-solutions.section-text ul li:nth-of-type(5)').text('Technologies');

		$('#above-the-fold #office-name').insertBefore('#above-the-fold .main-headline');

	}

	if ( windowWidth < 1200 ) {

		// homepage-sales-page

		$('#section-in-residence .main-headline').text('Work From Your Personal Tulane Office');

	}

	if ( windowWidth >= 1200 ) {

		// homepage-sales-page

		$('#section-in-residence .main-headline').text('What if you could work with us from your personal Tulane office?');

	}

});


//
// layout-setup
// centering content vertically within a container if window width is > 1280
//

var marginTop;
var additionalMargin = 20;

if ( windowWidth >= 1200 ) {

	// section-above-the-fold

	marginTop = $('#above-the-fold .container .row:first-of-type ').height() / 2;

	$('#above-the-fold .container .row:first-of-type ').css('margin-top', -marginTop);


	// section-text


		// section-text-1

		marginTop = $('.section-text.section-text-1 .row').height() / 2 - additionalMargin;

		console.log("Margin top to text section 1!");

		$('.section-text.section-text-1 .row').css('margin-top', -marginTop);

		// section-text-2

		marginTop = $('.section-text.section-text-2 .row').height() / 2 - additionalMargin;

		$('.section-text.section-text-2 .row').css('margin-top', -marginTop);

		// section-text-3

		marginTop = $('.section-text.section-text-3 .row').height() / 2;

		$('.section-text.section-text-3 .row').css('margin-top', -marginTop);


	// section-in-residence

	marginTop = $('#section-in-residence .container .row').height() / 2;

	$('#section-in-residence .container .row').css('margin-top', -marginTop);

}

$( window ).on('resize', function() {

	windowWidth = $( window ).width();

	if ( windowWidth < 1200 ) {

		// section-above-the-fold

		$('#above-the-fold .container .row:first-of-type ').css('margin-top', 0);

		// section-text

			// section-text-1

			$('.section-text.section-text-1 .row').css('margin-top', 0);

			// section-text-2

			$('.section-text.section-text-2 .row').css('margin-top', 0);

			// section-text-3

			$('.section-text.section-text-3 .row').css('margin-top', 0);


		// section-in-residence

		$('#section-in-residence .container .row').css('margin-top', 0);

	}

	if ( windowWidth >= 1200 ) {

		// section-above-the-fold

		marginTop = $('#above-the-fold .container .row:first-of-type ').height() / 2;

		$('#above-the-fold .container .row:first-of-type ').css('margin-top', -marginTop);


		// section-text


			// section-text-1

			marginTop = $('.section-text.section-text-1 .row').height() / 2 - additionalMargin;

			$('.section-text.section-text-1 .row').css('margin-top', -marginTop);

			// section-text-2

			marginTop = $('.section-text.section-text-2 .row').height() / 2 - additionalMargin;

			$('.section-text.section-text-2 .row').css('margin-top', -marginTop);

			// section-text-3

			marginTop = $('.section-text.section-text-3 .row').height() / 2;

			$('.section-text.section-text-3 .row').css('margin-top', -marginTop);


		// section-in-residence

		marginTop = $('#section-in-residence .container .row').height() / 2;

		$('#section-in-residence .container .row').css('margin-top', -marginTop);

	}

});