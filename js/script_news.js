
var windowWidth = $( window ).width();
var touch 		= ("ontouchstart" in document.documentElement);

//
// content-replacement
//

if ( windowWidth < 768 ) {

	$( '#page-title-area .page-headline' ).text( 'Stay Current With Tulane And The Industry' );

}

if ( windowWidth >= 768 ) {

	$( '#page-title-area .page-headline' ).text( 'Stay Current With Tulane and other Relevant Industry News' );

}

$( window ).on('resize', function() {

	windowWidth = $( window ).width();

	if ( windowWidth < 768 ) {

		$( '#page-title-area .page-headline' ).text( 'Stay Current With Tulane And The Industry' );

	}

	if ( windowWidth >= 768 ) {

		$( '#page-title-area .page-headline' ).text( 'Stay Current With Tulane and other Relevant Industry News' );

	}

});