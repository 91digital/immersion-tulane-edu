/*
 *
 */ 

$( document ).ready(function() {
    
	var touch = ("ontouchstart" in document.documentElement);

	//
	// site-navigation
	//

	// STICKY HEADER

	$(window).on('scroll', function() {

		var window_top		 = $(window).scrollTop();
        var top_position     = $('body').offset().top;
        var element_to_stick = $('.site-header');

        if (window_top > top_position) {

            element_to_stick.addClass('site-header-sticky');

        } else {

            element_to_stick.removeClass('site-header-sticky');

        }

	});




	// HOVER

	$('.site-nav .visible-links li a')
		.mouseover( function() {

			if (!touch) {

				$(this).addClass('visible-link-hover');

			}

		})
		.mouseout( function() {

			if (!touch) {
				
				$(this).removeClass('visible-link-hover');

			}

		});

	// RESPONSIVENESS

	// defining the variables

	var header = $('.site-header #header.container');
	var logo   = $('.site-header #header.container .logo-tulane');
	var btn    = $('.site-header #header.container .site-nav .btn-more');
	var vlinks = $('.site-header #header.container .site-nav .visible-links');
	var hlinks = $('.site-header #header.container .site-nav .hidden-links');
	var cta    = $('.site-header .btn-work-with-us');
	var breaks = [];

	// the nav function repsonsible for the responsiveness

	function updateNav() {

		// calculating the available space in the header

		if ( $(window).width() < 768 ) {

			if ( btn.hasClass('btn-more-hidden') ) {

				var availableSpace = header.width() - logo.width();

			} else {

				if ( $(window).width() >= 414 ) {

					var availableSpace = header.width() - logo.width() - btn.width();

				} else {

					var availableSpace = header.width() - logo.width();

				}

			}

		} else {

			if ( btn.hasClass('btn-more-hidden') ) {

				var availableSpace = header.width() - cta.width() - 16 - logo.width();

			} else {

				var availableSpace = header.width() - logo.width() - btn.width() - cta.width();

			}

		}

		// IF the width of the visible links > the availabel space

		if ( vlinks.width() > availableSpace ) {

			// THEN save the width in the breaks array

			breaks.push( vlinks.width() );

			// THEN move the last visible link to the hidden links

			vlinks.children().last().prev().prependTo( hlinks );

			// IF the more button is hidden

			if ( btn.hasClass('btn-more-hidden') ) {

				// THEN make the button visible

				btn.removeClass('btn-more-hidden');

			}

		} else {

			// IF the available space is > the width in the breaks array

			if ( availableSpace > breaks[breaks.length-1] ) {

				// THEN insert the first element from hlinks into vlinks 

				hlinks.children().first().insertAfter( vlinks.children().last().prev() );
				breaks.pop();

			}

			// IF the breaks array is empty

			if ( breaks.length < 1 ) {

				btn.addClass('btn-more-hidden');
				hlinks.addClass('hidden-links-hidden');

			}

		}

		// IF the width of the visible links is still > the available space

		if ( vlinks.width() > availableSpace ) {

			// THEN run the function again

			updateNav();

		}

		/*
		console.log('header width: ' + header.width());
		console.log('logo width: ' + logo.width());
		console.log('cta width: ' + cta.width());
		console.log('available space: ' + availableSpace);
		*/

	}

	// IF the browser windows is resized

	$( window ).resize( function() {

		updateNav();

	}); 

	// IF user clicks on the more button

	btn.on('click', function(event) {

		event.preventDefault();

		// THEN toggle the visibility of the drop down menu

		hlinks.toggleClass('hidden-links-hidden');

	});

	// run the function

	updateNav();

	// SHOW navigation

	$('.site-nav').removeClass('site-nav-hidden');
	
	
	//
	// scroll-to-top 
	//

	// IF user hovers over scroll-to-top icon -> THEN add class 'svg-hover'

	$('#svg-scroll-to-top')
		.mouseover( function() {

			if (!touch) {

				$(this).addClass('svg-hover');

			}

		})
		.mouseout( function() {

			if (!touch) {

				$(this).removeClass('svg-hover');

			}

		});

	// IF user clicks on scroll-to-top icon -> THEN scroll to top

	$('#svg-scroll-to-top').on('click', function() {

		$('html, body').animate({scrollTop: 0}, 800);
		return false;

	});

	//
	// site-footer
	//

	// IF user hovers over social icon -> add class 'svg-hover'

	$('.footer-social svg')
		.mouseover( function() {

			if (!touch) {
				
				$(this).addClass('svg-hover');
			
			}

		})
		.mouseout( function() {

			if (!touch) {
				
				$(this).removeClass('svg-hover');

			}

		});

});